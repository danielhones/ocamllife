val horiz_bar : string
val vert_bar : string
val top_left : string
val top_rt : string
val btm_left : string
val btm_rt : string

val sleepf : float -> unit
val clear_screen : unit -> unit
val format_grid : Life.cell list list -> string
val animate : ?toroidal:bool -> Life.grid -> float -> int -> unit
