type cell = Full | Empty
type row = cell list
type grid = row list
type neighbor_grid = int list list
type coord = int * int
type cell_and_neighbors = cell * int

val full : cell
val empty : cell
val is_empty : cell -> bool
val is_full : cell -> bool

val next_from_count : cell_and_neighbors -> cell
val next_state : ?toroidal:bool -> cell list list -> cell list list
