(*
TODO: Add an option to use a toroidal topology so that the edges wrap around
      like Pacman
TODO: Consider using a different data structure that allows a grid of infinite
      size, and display only the state within a window of a certain size and 
      position on the grid
*)

type cell = Full | Empty
type row = cell list
type grid = row list
type neighbor_grid = int list list
type coord = int * int
type cell_and_neighbors = cell * int

let empty = Empty
let full = Full
let is_empty = (=) empty
let is_full = (=) full

let pick_cell grid (row, col) =
  List.nth (List.nth grid row) col

let toroidal_neighbors grid (row, col) =
  let height = List.length grid in
  let width = List.length (List.nth grid 0) in
  let (%) x m =
    if x < 0 then (m + x) mod m else x mod m in
  let all_nbrs = [(row - 1, col - 1); (row - 1, col); (row - 1, col + 1);
                  (row, col - 1); (row, col + 1);
                  (row + 1, col - 1); (row + 1, col); (row + 1, col + 1)] in
  List.map (fun (r, c) -> (r % height, c % width))
           all_nbrs

let dead_edge_neighbors grid (row, col) =
  let height = List.length grid in
  let width = List.length (List.nth grid 0) in
  let all_nbrs = [(row - 1, col - 1); (row - 1, col); (row - 1, col + 1);
                  (row, col - 1); (row, col + 1);
                  (row + 1, col - 1); (row + 1, col); (row + 1, col + 1)] in
  List.filter (fun (r, c) -> r >= 0 && c >= 0 && r < height && c < width)
              all_nbrs

(* Given a grid and coordinate in it, return list of the neighboring
   cells.  The length of the returned list will depend on the position
   in the grid, for example a corner cell would return a list of only 3
   neighbors *)
let neighbor_cells grid (row, col) ~ngbr_fun =
  let neighbors = ngbr_fun grid (row, col) in
  List.map (pick_cell grid) neighbors

let neighbor_count grid pos ~ngbr_fun =
  let cell = pick_cell grid pos in
  let neighbors = List.fold_left
                    (fun acc cell -> acc + if is_full cell then 1 else 0)
                    0
                    (neighbor_cells grid pos ~ngbr_fun) in
  (cell, neighbors)

let neighbor_grid grid ~ngbr_fun =
  List.mapi
    (fun i row ->
      (List.mapi
         (fun j x -> neighbor_count grid (i, j) ~ngbr_fun)
         row))
    grid

(* Given current value and the  neighbor count of a cell, return value for
   that cell for the next generation *)
let next_from_count (cell, neighbors) =
  match neighbors with
  | 2 -> if is_full cell then full else empty
  | 3 -> full
  | _ -> empty

let next_state ?(toroidal=false) grid =
  let ngbr_fun = if toroidal then toroidal_neighbors else dead_edge_neighbors in
  let neighbors = neighbor_grid grid ~ngbr_fun in
  let next_grid = List.map (fun r -> List.map next_from_count r) neighbors in
  next_grid
