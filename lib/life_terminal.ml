(* Some manually encoded Unicode characters for box drawing: *)
let horiz_bar = "\xe2\x94\x80"
let vert_bar = "\xe2\x94\x82"
let top_left = "\xe2\x94\x8c"
let top_rt = "\xe2\x94\x90"
let btm_left = "\xe2\x94\x94"
let btm_rt = "\xe2\x94\x98"

let full_char =  "O"
let empty_char = " "

let concat = String.concat ""

let rec join xs =
  match xs with
  | [] -> ""
  | (hd :: rest) -> hd ^ (join rest)

let sleepf (sec: float) =
  ignore (Unix.select [] [] [] sec)

let clear_screen () =
  print_string "\x1b[2J\x1b[;H"

let format_grid grid =
  let horiz_line = (List.fold_left
                       (^)
                       ""
                       (List.map (fun _ -> horiz_bar) grid)) in
  let top_horiz = concat [top_left; horiz_line; top_rt; "\n"] in
  let bottom_horiz = concat [btm_left; horiz_line; btm_rt; "\n"] in
  let format_row r =
    join (List.map (fun i -> if Life.is_full i
                             then full_char
                             else empty_char) r) in
  let rec do_format grid =
    match grid with
    | [] ->  ""
    | (row :: rest) -> concat [vert_bar; (format_row row); vert_bar;
                               "\n"; do_format rest] in
  concat [top_horiz; do_format grid; bottom_horiz]

let rec animate ?(toroidal=false) grid sleep_sec counter =
  let next = Life.next_state ~toroidal grid in
  let print_grid grid counter =
    clear_screen ();
    Printf.printf "Generation: %i\n" counter;
    print_endline @@ format_grid grid; in
  print_grid grid counter;
  sleepf(sleep_sec);
  if grid = next then (
    print_grid next (counter + 1);
    print_endline "Static";
  ) else (
    animate ~toroidal next sleep_sec (counter + 1)
  )
