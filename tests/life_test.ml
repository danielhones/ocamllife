open OUnit
open Life

let test_next_from_count =
  "test_next_from_count" >::: [
      "underpopulated returns Empty" >:: (fun _ ->
        assert_equal Empty (next_from_count (Full, 0));
        assert_equal Empty (next_from_count (Full, 1))
      );
      "full with 2 neighbors returns Full" >:: (fun _ ->
        assert_equal Full (next_from_count (Full, 2));
      );
      "empty with 2 neighbors returns Empty" >:: (fun _ ->
        assert_equal Empty (next_from_count (Empty, 2))
      );
      "empty with 3 neighbors returns Full" >:: (fun _ ->
        assert_equal Full (next_from_count (Empty, 3))
      );
      "full with 3 neighbors retursn Full" >:: (fun _ ->
        assert_equal Full (next_from_count (Full, 3))
      );
      "with 4+ neighbors returns Empty" >:: (fun _ ->
        for i = 4 to 8 do
          assert_equal Empty (next_from_count (Full, i));
          assert_equal Empty (next_from_count (Empty, i));
        done
      );
    ]

let test_next_state =
  let x = Full in
  let o = Empty in
  "test_next_state" >::: [
      "blinker" >:: (fun _ ->
        let grid = [[o; o; o];
                    [x; x; x];
                    [o; o; o]] in
        let expected = [[o; x; o];
                        [o; x; o];
                        [o; x; o]] in
        assert_equal expected (next_state grid)
      );
      "toad" >:: (fun _ ->
        let grid = [[o; o; o; o; o; o];
                    [o; o; o; o; o; o];
                    [o; o; x; x; x; o];
                    [o; x; x; x; o; o];
                    [o; o; o; o; o; o];
                    [o; o; o; o; o; o]] in
        let expected = [[o; o; o; o; o; o];
                        [o; o; o; x; o; o];
                        [o; x; o; o; x; o];
                        [o; x; o; o; x; o];
                        [o; o; x; o; o; o];
                        [o; o; o; o; o; o]] in
        assert_equal expected (next_state grid)
      );
      "r-pentomino" >:: (fun _ ->
        let grid = [[o; o; o; o; o; o];
                    [o; o; o; x; x; o];
                    [o; o; x; x; o; o];
                    [o; o; o; x; o; o];
                    [o; o; o; o; o; o];
                    [o; o; o; o; o; o]] in
        let expected = [[o; o; o; o; o; o];
                        [o; o; x; x; x; o];
                        [o; o; x; o; o; o];
                        [o; o; x; x; o; o];
                        [o; o; o; o; o; o];
                        [o; o; o; o; o; o]] in
        assert_equal expected (next_state grid)
      );
      "diehard" >:: (fun _ ->
        let grid = [[o; o; o; o; o; o; o; o; o; o];
                    [o; o; o; o; o; o; o; o; o; o];
                    [o; o; o; o; o; o; o; o; o; o];
                    [o; o; o; o; o; o; o; x; o; o];
                    [o; x; x; o; o; o; o; o; o; o];
                    [o; o; x; o; o; o; x; x; x; o];
                    [o; o; o; o; o; o; o; o; o; o];
                    [o; o; o; o; o; o; o; o; o; o];
                    [o; o; o; o; o; o; o; o; o; o];
                    [o; o; o; o; o; o; o; o; o; o]] in
        let expected = [[o; o; o; o; o; o; o; o; o; o];
                        [o; o; o; o; o; o; o; o; o; o];
                        [o; o; o; o; o; o; o; o; o; o];
                        [o; o; o; o; o; o; o; o; o; o];
                        [o; x; x; o; o; o; x; o; x; o];
                        [o; x; x; o; o; o; o; x; o; o];
                        [o; o; o; o; o; o; o; x; o; o];
                        [o; o; o; o; o; o; o; o; o; o];
                        [o; o; o; o; o; o; o; o; o; o];
                        [o; o; o; o; o; o; o; o; o; o]] in
        assert_equal expected (next_state grid)
      );
      "static square" >:: (fun _ ->
        let grid = [[o; o; o; o];
                    [o; x; x; o];
                    [o; x; x; o];
                    [o; o; o; o]] in
        let expected = [[o; o; o; o];
                        [o; x; x; o];
                        [o; x; x; o];
                        [o; o; o; o]] in
        assert_equal expected (next_state grid)
      );
      "static oval, non-square grid" >:: (fun _ ->
        let grid = [[o; x; o];
                    [x; o; x];
                    [x; o; x];
                    [o; x; o]] in
        let expected = [[o; x; o];
                        [x; o; x];
                        [x; o; x];
                        [o; x; o]] in
        assert_equal expected (next_state grid)
      );
      "edge case" >:: (fun _ ->
        let grid = [[x; x; x; o];
                    [o; o; o; o];
                    [o; o; o; o];
                    [o; o; o; o]] in
        let expected = [[o; x; o; o];
                        [o; x; o; o];
                        [o; o; o; o];
                        [o; o; o; o]] in
        assert_equal expected (next_state grid)
      )
    ]

let test =
  "Life" >::: [
      test_next_from_count;
      test_next_state;
    ]
