open OUnit
open Life


let suite =
  "suite" >::: [
      Life_test.test
    ]


let _ = run_test_tt_main suite
