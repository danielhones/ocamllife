# Game of Life in OCaml

This is just a small project I'm using to learn OCaml.  It will start simply and add features as I learn the language, with the eventual goal of adding GUI support.  At the moment it's just ASCII art in the terminal.


## Building and running

I developed and tested this with OCaml 4.06.0 but I believe it should also run in most other versions.

```
cd <path to repo>
make
./life
```

It runs indefinitely, so use `ctrl+c` to stop it.


## Changing the initial state

Currently the initial state is hard-coded in `bin/main.ml` in the main function.  To use a different state you would edit it there and then rebuild.  That grid uses an `o` to indicate an empty cell and an `x` to indicate a full one.

Soon I'll add a feature to define the initial state as ASCII art in a file and take a command-line argument indicating which file to use.
