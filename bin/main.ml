let start_grid =
  let x = Life.full in
  let o = Life.empty in
  [[o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; x; o; o; o; o; o; o];
   [o; o; o; x; x; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; x; o; o; o; x; x; x; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o];
   [o; o; o; o; o; o; o; o; o; o; o; o; o; o; o; o]]

let main () =
  let sleep_sec = ref 0.15 in
  let toroidal = ref false in
  let args = [
      ("-s", Arg.Float (fun x -> sleep_sec := x),
       "Number (float) of seconds to sleep between generations.  \
        Default is 0.15 seconds.");
      ("-t", Arg.Set toroidal,
       "Use toroidal topology where edges of the grid wrap to the \
        other side.");
    ] in
  let usage = "Conway's Game of Life" in
  Arg.parse args print_endline usage;
  Life_terminal.animate ~toroidal:!toroidal start_grid sleep_sec.contents 0

let () = main ()


(*
TODO: Add serialize/deserialize methods for reading/writing state to a string
TODO: Add ability to read initial state from file 
TODO: Take argument on command line that accepts a filename
*)
