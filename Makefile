OC = ocamlfind ocamlopt

LIB_DIR_FLAGS = -I lib
FLAGS = -g $(LIB_DIR_FLAGS) -linkpkg
PACKAGES = unix
LIB_SOURCES = lib/life.mli lib/life.ml lib/life_terminal.mli lib/life_terminal.ml
EXE_SOURCES = bin/main.ml
EXECUTABLE = life

TEST_FLAGS = -g $(LIB_DIR_FLAGS) -I tests -linkpkg
TEST_PACKAGES = $(PACKAGES),oUnit
TEST_SOURCES = tests/life_test.ml
TEST_EXE_SOURCES = tests/test.ml
TEST_EXE = run_tests

all: build

build: clean
	$(OC) $(FLAGS) -package $(PACKAGES) -o $(EXECUTABLE) $(LIB_SOURCES) $(EXE_SOURCES)

test: clean
	@$(OC) $(TEST_FLAGS) -package $(TEST_PACKAGES) -o $(TEST_EXE) $(LIB_SOURCES) $(TEST_SOURCES) $(TEST_EXE_SOURCES)
	@./$(TEST_EXE)

test-watch:
	while inotifywait -e close_write **/*.ml; do clear && make test; done

clean:
	rm -rf **/*.cm* **/*.o

.PHONY: build test test-watch clean
